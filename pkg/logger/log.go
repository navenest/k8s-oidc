package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"log"
	"os"
	"strconv"
)

type logConfig struct {
	Level            string            `json:"level"`
	Encoding         string            `json:"encoding"`
	OutputPaths      []string          `json:"outputPaths"`
	ErrorOutputPaths []string          `json:"errorOutputPaths"`
	InitialFields    map[string]string `json:"initialFields"`
	EncoderConfig    encoderConfig     `json:"encoderConfig"`
}

type encoderConfig struct {
	MessageKey   string `json:"messageKey"`
	LevelKey     string `json:"levelKey"`
	LevelEncoder string `json:"levelEncoder"`
}

func initZapLog() *zap.Logger {

	var logLevel zapcore.Level
	var developmentMode bool
	var err error
	if value, ok := os.LookupEnv("LOG_LEVEL"); ok {
		logLevel, err = zapcore.ParseLevel(value)
		if err != nil {
			log.Fatal("Unable to parse Log Level")
		}
	} else {
		logLevel, err = zapcore.ParseLevel("info")
		if err != nil {
			log.Fatal("Unable to parse Log Level")
		}
	}
	if value, ok := os.LookupEnv("DEVELOPMENT_MODE"); ok {
		developmentMode, _ = strconv.ParseBool(value)
	} else {
		developmentMode = false
	}
	config := zap.Config{
		Level:             zap.NewAtomicLevelAt(logLevel),
		Development:       developmentMode,
		DisableCaller:     false,
		DisableStacktrace: false,
		Encoding:          "json",
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:   "time",
			LevelKey:  "level",
			NameKey:   "logger",
			CallerKey: "caller",
			//FunctionKey:    "function",
			MessageKey:     "message",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.CapitalLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
		InitialFields:    map[string]interface{}{"app": "k8s-oidc"},
	}
	if err != nil {
		panic(err)
	}
	logger, err := config.Build()
	if err != nil {
		panic(err)
	}
	return logger
}

var Logger *zap.Logger

func init() {
	Logger = initZapLog()
	Logger.Info("logger initialized")
}

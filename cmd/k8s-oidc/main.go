package main

import (
	"gitlab.com/navenest/k8s-oidc/internal/oidcGetter"
	logz "gitlab.com/navenest/k8s-oidc/pkg/logger"
	"gitlab.com/navenest/k8s-oidc/web"
	"os"
)

func main() {
	logz.Logger.Info("logger construction succeeded")

	// Location for Kubernetes tokenFile
	var tokenFile string
	if value, ok := os.LookupEnv("TOKEN_FILE"); ok {
		tokenFile = value
	} else {
		tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"
	}

	var rootCAFile string
	if value, ok := os.LookupEnv("ROOT_CA_FILE"); ok {
		rootCAFile = value
	} else {
		rootCAFile = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
	}

	host, port := os.Getenv("KUBERNETES_SERVICE_HOST"), os.Getenv("KUBERNETES_SERVICE_PORT")

	go oidcGetter.GetOIDCConfigs(host, port, tokenFile, rootCAFile)

	webserver := web.App{}
	webserver.Initialize()
	webserver.Run(":8080")
}

package oidcGetter

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	logz "gitlab.com/navenest/k8s-oidc/pkg/logger"
	"go.uber.org/zap"
	"io"
	"net/http"
	"os"
	"time"
)

var k8sClient *http.Client

var OIDCConfig string
var JWKSConfig string

func getOIDC(path, jwt string) string {
	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		logz.Logger.Error("error building http request",
			zap.Error(err))
	}
	req.Header = map[string][]string{
		"Authorization": {fmt.Sprintf("Bearer %s", jwt)},
	}
	response, err := k8sClient.Do(req)
	if err != nil {
		logz.Logger.Error("error making http request",
			zap.Error(err))
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			logz.Logger.Error("Unable to close body")
		}
	}(response.Body)

	body, err := io.ReadAll(response.Body)
	if err != nil {
		logz.Logger.Error("Unable to read body",
			zap.Error(err))
	}
	return string(body)
}

func GetOIDCConfigs(host, port, tokenFilePath, rootCAFilePath string) {
	basePath := "https://" + host + ":" + port
	token, err := os.ReadFile(tokenFilePath)

	rootCAs, err := x509.SystemCertPool()
	if err != nil {
		logz.Logger.Error("unable to load system certs",
			zap.Error(err))
	}
	if rootCAs == nil {
		rootCAs = x509.NewCertPool()
	}

	ca, err := os.ReadFile(rootCAFilePath)
	if err != nil {
		logz.Logger.Error("unable to read ca",
			zap.Error(err))
	}

	if ok := rootCAs.AppendCertsFromPEM(ca); !ok {
		logz.Logger.Info("using system certs only")
	}

	config := &tls.Config{
		RootCAs: rootCAs,
	}
	tr := &http.Transport{TLSClientConfig: config}
	k8sClient = &http.Client{Transport: tr}

	if err != nil {
		logz.Logger.Error("unable to read file",
			zap.Error(err))
	}
	for {
		openIDConfig := getOIDC(basePath+"/.well-known/openid-configuration", string(token))
		jwks := getOIDC(basePath+"/openid/v1/jwks", string(token))
		OIDCConfig = openIDConfig
		JWKSConfig = jwks
		time.Sleep(60 * time.Second)
	}
}

FROM golang:1.19 as golang-builder

WORKDIR /usr/local/go/src/k8s-oidc

# Copy go mod and sum files
COPY go.mod go.sum ./
COPY . .
# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

#RUN go test -json ./...



# RUN CGO_ENABLED=0 go test ./... -v -cover
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main ./cmd/k8s-oidc/

######## Start a new stage from scratch #######
FROM debian:bullseye-slim
WORKDIR /app/

RUN mkdir -p dist
RUN apt update && apt install -y ca-certificates

COPY --from=golang-builder /usr/local/go/src/k8s-oidc/main .
#RUN chmod +x main


# Command to run the executable
CMD ["./main"]
package web

import (
	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"gitlab.com/navenest/k8s-oidc/internal/oidcGetter"
	logz "gitlab.com/navenest/k8s-oidc/pkg/logger"
	"go.uber.org/zap"
	"net/http"
	"time"
)

type App struct {
	Router *gin.Engine
	//ApiRouter *echo.Group
	//apiv1 *apiv1.API
}

type health struct {
	Status string `json:"status"`
}

func (web *App) Initialize() {

	web.Router = gin.New()
	web.Router.Use(ginzap.GinzapWithConfig(logz.Logger, &ginzap.Config{
		TimeFormat: time.RFC3339Nano,
		UTC:        true,
		SkipPaths:  []string{"/healthcheck"},
	}))
	web.initializeRoutes()
	//p := ginprometheus.NewPrometheus("echo", nil)
	//p.Use(web.Router)
	//web.Router.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
	//	Skipper: func(c echo.Context) bool {
	//		if strings.HasPrefix(c.Request().RequestURI, "/healthcheck") || strings.HasPrefix(c.Request().RequestURI, "/metrics") {
	//			return true
	//		}
	//		return false
	//	},
	//}))
	//web.Router.Use(middleware.GzipWithConfig(middleware.GzipConfig{
	//	Skipper: func(c echo.Context) bool {
	//		return strings.Contains(c.Path(), "metrics") // Change "metrics" for your own path
	//	},
	//}))
	//web.Router.HTTPErrorHandler = customHTTPErrorHandler
}

func (web *App) Run(addr string) {
	if addr == "" {
		addr = ":8080"
	}
	err := web.Router.Run(addr)
	if err != nil {
		logz.Logger.Fatal("Issues with webserver: "+err.Error(),
			zap.Error(err))
	}
}

func (web *App) initializeRoutes() {
	web.Router.GET("/healthcheck", web.healthcheck)
	web.Router.GET("/.well-known/openid-configuration", web.oidc)
	web.Router.GET("/openid/v1/jwks", web.jwks)
}

func (web *App) oidc(c *gin.Context) {
	c.String(200, oidcGetter.OIDCConfig)
}

func (web *App) jwks(c *gin.Context) {
	c.String(200, oidcGetter.JWKSConfig)
}

func (web *App) healthcheck(c *gin.Context) {
	data := health{
		Status: "UP",
	}
	c.JSON(http.StatusOK, data)
}
